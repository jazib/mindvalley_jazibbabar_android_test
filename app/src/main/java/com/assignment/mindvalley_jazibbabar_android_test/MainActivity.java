package com.assignment.mindvalley_jazibbabar_android_test;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.assignment.awesomelibrary.loader.ImageLoader;
import com.assignment.awesomelibrary.loader.JSONArrayLoader;
import com.assignment.awesomelibrary.ResponseListener;
import com.assignment.awesomelibrary.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pla.lib.MultiColumnPullToRefreshListView;


public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";
    private static final String END_POINT = "http://pastebin.com/raw/wgkJgazE";

    private MultiColumnPullToRefreshListView mListView;
    private ImagesListAdapter mAdapter;
    private ArrayList<ImageInfo> mImagesList;
    private JSONArrayLoader mJsonLoader;

    private PullToRefreshLisener mPullToRefreshLisener;

    private ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImagesList = new ArrayList<>();
        mAdapter = new ImagesListAdapter(this, mImagesList);
        mListView = (MultiColumnPullToRefreshListView) findViewById(R.id.list);
        mPullToRefreshLisener = new PullToRefreshLisener();

        mListView.setAdapter(mAdapter);
        mListView.setOnRefreshListener(mPullToRefreshLisener);

        mImageLoader = new ImageLoader();

        mJsonLoader = new JSONArrayLoader(END_POINT);
        mJsonLoader.loadJSONArray(END_POINT, new ResponseListener<JSONArray>() {
            @Override
            public void onSuccesfullResponse(JSONArray jsonArray) {
                Utils.LogD(TAG, "JSON: " + jsonArray);
                fillData(jsonArray);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onErrorResponse(Exception e) {
                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }


    public void fillData(JSONArray data) {
        try {
            for (int i = 0; i < data.length(); i++) {
                JSONObject jsonObject = (JSONObject) data.get(i);
                ImageInfo imageInfo = new ImageInfo(jsonObject);
                mImagesList.add(imageInfo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),
                    "Error: " + e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }

    public class PullToRefreshLisener implements MultiColumnPullToRefreshListView.OnRefreshListener {

        @Override
        public void onRefresh() {
            refreshData();
        }
    }

    private void refreshData() {
        mImagesList = new ArrayList<>();
        mJsonLoader.loadJSONArray(END_POINT,
                new ResponseListener<JSONArray>() {
                    @Override
                    public void onSuccesfullResponse(JSONArray jsonArray) {
                        Utils.LogD(TAG, jsonArray.toString());
                        fillData(jsonArray);
                        mAdapter.notifyDataSetChanged();
                        mListView.onRefreshComplete();
                    }

                    @Override
                    public void onErrorResponse(Exception e) {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        mListView.onRefreshComplete();
                    }
                });

    }

    public class ImagesListAdapter extends BaseAdapter {
        private List<ImageInfo> imagesList;
        private Context mContext;

        public ImagesListAdapter(Context context, List data) {
            mContext = context;
            imagesList = data;
        }

        @Override
        public int getCount() {
            return imagesList.size();
        }

        @Override
        public Object getItem(int i) {
            return imagesList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;
            ImageInfo imageInfo = imagesList.get(i);

            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(R.layout.list_item, viewGroup, false);
                viewHolder.mainImage = (ImageView) convertView.findViewById(R.id.image);
                viewHolder.name = (TextView) convertView.findViewById(R.id.name);
                viewHolder.categories = (TextView) convertView.findViewById(R.id.categories);
                viewHolder.numOfLikes = (TextView) convertView.findViewById(R.id.num_of_likes);
                viewHolder.profileImage = (CircleImageView)convertView.findViewById(R.id.profile_image);
                setMainImage(viewHolder, imageInfo, true);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                setMainImage(viewHolder, imageInfo, false);
            }

            if (imageInfo != null) {
                viewHolder.name.setText(imageInfo.getName());
                viewHolder.numOfLikes.setText(""+imageInfo.getLikes());
                String cats = "";
                for(int catIterator  = 0; catIterator < imageInfo.getCategories().size(); catIterator++){
                    if(catIterator == imageInfo.getCategories().size()-1 ){
                        cats+=imageInfo.getCategories().get(catIterator);
                    }
                    else {
                        cats += imageInfo.getCategories().get(catIterator) + ", ";
                    }
                }
                viewHolder.categories.setText(cats);

                mImageLoader.loadImage(imageInfo.getProfileImageSmallUrl(), viewHolder.profileImage, new ResponseListener<Bitmap>() {
                    @Override
                    public void onSuccesfullResponse(Bitmap bitmap) {
                        Utils.LogD(TAG, "onSuccesfullResponse");
                        viewHolder.profileImage.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onErrorResponse(Exception e) {
                        e.printStackTrace();
                    }
                });

            }
            return convertView;
        }

        private void setMainImage(final ViewHolder viewHolder, ImageInfo imageInfo, final boolean loadWithAnim) {
            if(imageInfo == null) {
                return;
            }

            mImageLoader.loadImage(imageInfo.getUrlSmall(),viewHolder.mainImage, new ResponseListener<Bitmap>() {
                @Override
                public void onSuccesfullResponse(Bitmap bitmap) {
                    Utils.LogD(TAG, "onSuccesfullResponse");
                    viewHolder.mainImage.setImageBitmap(bitmap);
                    if(loadWithAnim) {
                        Animation myFadeInAnimation =  AnimationUtils.loadAnimation(mContext, R.anim.fadein);
                        viewHolder.mainImage.startAnimation(myFadeInAnimation);
                    }

                }

                @Override
                public void onErrorResponse(Exception e) {
                    e.printStackTrace();
                }
            });
        }


        public class ViewHolder {
            ImageView mainImage;
            CircleImageView profileImage;
            TextView name;
            TextView categories;
            TextView numOfLikes;

        }
    }

}
