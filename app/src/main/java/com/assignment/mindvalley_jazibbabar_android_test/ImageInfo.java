package com.assignment.mindvalley_jazibbabar_android_test;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jazib on 8/7/2016.
 */
public class ImageInfo {
    private String id;
    private String created_at;
    private int width;
    private int height;
    private String color;
    private int likes;
    private boolean likedByUser;
    private String urlRegular;
    private String urlSmall;
    private ArrayList<String> categories = new ArrayList<>();
    private String name;
    private String profileImageSmallUrl;
    private final static String KEY_ID = "id";
    private final static String KEY_URLS = "urls";
    private final static String KEY_REGULAR_URL = "regular";
    private final static String KEY_SMALL_URL = "small";
    private final static String KEY_LIKES = "likes";
    private final static String KEY_USER = "user";
    private final static String KEY_NAME =  "name";
    private final static String KEY_PROFILE_IMAGE =  "profile_image";
    private final static String KEY_PROFILE_IMAGE_SMALL =  "small";
    private final static String KEY_CATEGORIES =  "categories";
    private final static String KEY_CATEGORY_TITLE =  "title";

    public ImageInfo(JSONObject jsonObject){
        try {
            setId(jsonObject.getString(KEY_ID));
            JSONObject urls = jsonObject.getJSONObject(KEY_URLS);
            setUrlSmall(urls.getString(KEY_SMALL_URL));
            setLikes(jsonObject.getInt(KEY_LIKES));

            JSONObject userJSONObject = jsonObject.getJSONObject(KEY_USER);
            setName(userJSONObject.getString(KEY_NAME));

            JSONObject userProfileUrls = userJSONObject.getJSONObject(KEY_PROFILE_IMAGE);
            setProfileImageSmallUrl(userProfileUrls.getString(KEY_PROFILE_IMAGE_SMALL));

            JSONArray categoriesJSONObject = jsonObject.getJSONArray(KEY_CATEGORIES);
            for (int i = 0; i < categoriesJSONObject.length(); i++) {
                JSONObject category = (JSONObject) categoriesJSONObject.get(i);
                categories.add(category.getString(KEY_CATEGORY_TITLE));
            }

        } catch (JSONException e) {
            e.printStackTrace();
            ;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public String getUrlRegular() {
        return urlRegular;
    }

    public void setUrlRegular(String urlRegular) {
        this.urlRegular = urlRegular;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageSmallUrl() {
        return profileImageSmallUrl;
    }

    public void setProfileImageSmallUrl(String profileImageSmallUrl) {
        this.profileImageSmallUrl = profileImageSmallUrl;
    }

    public String getUrlSmall() {
        return urlSmall;
    }

    public void setUrlSmall(String urlSmall) {
        this.urlSmall = urlSmall;
    }
}
