package com.assignment.mindvalley_jazibbabar_android_test;

import android.app.Application;

/**
 * Created by Jazib on 8/6/2016.
 */
public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();


    private static AppController mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

}