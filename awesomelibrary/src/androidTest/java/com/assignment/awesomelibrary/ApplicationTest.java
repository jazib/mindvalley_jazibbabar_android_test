package com.assignment.awesomelibrary;

import android.app.Application;
import android.support.test.runner.AndroidJUnit4;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.assignment.awesomelibrary.loader.JSONArrayLoader;

import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class ApplicationTest extends ApplicationTestCase<Application> {
    static final String END_POINT = "http://pastebin.com/raw/wgkJgazE";
    public ApplicationTest() {
        super(Application.class);
    }

    @Test
    public void jsonArrayMemoryLimitTest(){
        final JSONArrayLoader loader = new JSONArrayLoader(END_POINT);
        final CountDownLatch signal = new CountDownLatch(1);
        loader.loadJSONArray(END_POINT, new ResponseListener<JSONArray>() {
            @Override
            public void onSuccesfullResponse(JSONArray jsonArray) {
                assertThat(loader.getMemoryCache().isSizeGreater(), is(false));
                signal.countDown();
            }

            @Override
            public void onErrorResponse(Exception e) {
                e.printStackTrace();
                assertThat(0, is(1));
                signal.countDown();
            }
        });
        try {
            signal.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            assertThat(0, is(1));
        }
    }




}