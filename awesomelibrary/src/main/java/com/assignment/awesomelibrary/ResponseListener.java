package com.assignment.awesomelibrary;

/**
 * Created by Jazib on 8/7/2016.
 */
public interface ResponseListener <T> {
    public void onSuccesfullResponse(T t);
    public void onErrorResponse(Exception e);
}
