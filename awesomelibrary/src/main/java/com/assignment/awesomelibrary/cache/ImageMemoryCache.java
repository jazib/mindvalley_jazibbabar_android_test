package com.assignment.awesomelibrary.cache;

import android.graphics.Bitmap;

/**
 * Created by Jazib on 8/7/2016.
 */
public class ImageMemoryCache extends MemoryCache <Bitmap> {

    public ImageMemoryCache() {
        limit = Runtime.getRuntime().maxMemory() / 4;
    }
    @Override
    public Bitmap get(String id)  {
        try {
            if(!cache.containsKey(id)) {
                return null;
            }

            return (Bitmap) cache.get(id);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void put(String id, Bitmap bitmap)  {
        try {
            if(cache.containsKey(id)) {
                size -= getSizeInBytes(cache.get(id));
            }
            cache.put(id, bitmap);
            size += getSizeInBytes(bitmap);
            checkSize();
        } catch(Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    long getSizeInBytes(Bitmap bitmap) {
        if(bitmap == null) {
            return 0;
        }
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

}
