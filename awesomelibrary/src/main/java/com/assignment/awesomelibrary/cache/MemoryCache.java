package com.assignment.awesomelibrary.cache;


import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class MemoryCache <T> {

    private static final String TAG = "MemoryCache";

    protected Map<String, T > cache =
            Collections.synchronizedMap(new LinkedHashMap<String, T>(10, 1.5f, true));

    protected long size = 0;
    protected long limit = Runtime.getRuntime().maxMemory() / 4;

    public abstract T get(String id);

    public abstract void put(String id, T t);

    protected void checkSize() {
        if(size > limit) {
            Iterator<Map.Entry<String, T>> iter = cache.entrySet().iterator();
            while(iter.hasNext()) {
                Map.Entry<String, T> entry = iter.next();
                size -= getSizeInBytes(entry.getValue());
                iter.remove();
                if(size <= limit) {
                    break;
                }
            }
        }
    }

    public void clear() {
        try {
            cache.clear();
            size = 0;
        } catch(NullPointerException e) {
            e.printStackTrace();
        }
    }

    abstract long getSizeInBytes(T t);

    public boolean isSizeGreater(){
        if(size > limit) {
            return true;
        }
        return false;
    }
}
