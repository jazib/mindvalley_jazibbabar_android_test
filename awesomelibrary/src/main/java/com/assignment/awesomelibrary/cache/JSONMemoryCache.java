package com.assignment.awesomelibrary.cache;

import org.json.JSONArray;

/**
 * Created by Jazib on 8/7/2016.
 */
public class JSONMemoryCache extends MemoryCache <JSONArray> {

    public JSONMemoryCache() {
        limit = Runtime.getRuntime().maxMemory() / 16;
    }
    @Override
    public JSONArray get(String id)  {
        try {
            if(!cache.containsKey(id)) {
                return null;
            }

            return (JSONArray) cache.get(id);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void put(String id, JSONArray bitmap)  {
        try {
            if(cache.containsKey(id)) {
                size -= getSizeInBytes(cache.get(id));
            }
            cache.put(id, bitmap);
            size += getSizeInBytes(bitmap);
            checkSize();
        } catch(Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    long getSizeInBytes(JSONArray jsonArray) {
        if(jsonArray == null) {
            return 0;
        }
        return jsonArray.toString().getBytes().length;
    }

}
