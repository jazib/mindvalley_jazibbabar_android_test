package com.assignment.awesomelibrary.loader;

import android.content.Context;
import android.graphics.Bitmap;
import android.location.GpsStatus;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.assignment.awesomelibrary.cache.MemoryCache;
import com.assignment.awesomelibrary.ResponseListener;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Jazib on 8/8/2016.
 */
public abstract class Loader <T> {

    private static final String TAG = "Loader";
    public abstract T decodeItem(InputStream in);

    protected MemoryCache<T> memoryCache;

//    protected String mUrl;
//    protected ResponseListener<T> mListener;


    public abstract void handleOnPostExecute( T result, String url,Loader.LoaderAsyncTask task,  ResponseListener<T> listener);


    protected void load( ResponseListener<T> listener, String url) {
        LoaderAsyncTask task = new LoaderAsyncTask(listener);
        task.execute(url);
    }
    class LoaderAsyncTask extends AsyncTask<Object, Void, T> {
        private WeakReference<ImageView> imageViewWeakReference;
        String url;
        ResponseListener<T> listener;

        public LoaderAsyncTask() {

        }
        public LoaderAsyncTask( ResponseListener<T> listener) {
            this.listener = listener;
        }
        public LoaderAsyncTask(ImageView imageView,  ResponseListener<T> listener) {
            imageViewWeakReference = new WeakReference<ImageView>(imageView);
            this.listener = listener;
        }

        public String getUrl(){
            return this.url;
        }


        @Override
        protected T doInBackground(Object... strings) {
            this.url = (String) strings[0];
            StringBuilder result = new StringBuilder();

            try {
                URL url = new URL(this.url);
                HttpURLConnection mUrlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(mUrlConnection.getInputStream());
                T t = decodeItem(in);
                return t;
            }catch( Exception e) {
                e.printStackTrace();
                this.listener.onErrorResponse(e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(T result) {
            handleOnPostExecute(result,this.url, this, this.listener);
        }

        public WeakReference<ImageView> getImageViewWeakReference() {
            return imageViewWeakReference;
        }
    }


}
