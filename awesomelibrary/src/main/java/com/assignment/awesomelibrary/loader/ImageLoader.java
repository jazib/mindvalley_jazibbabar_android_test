package com.assignment.awesomelibrary.loader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.assignment.awesomelibrary.cache.ImageMemoryCache;
import com.assignment.awesomelibrary.ResponseListener;
import com.assignment.awesomelibrary.Utils;

import java.io.InputStream;
import java.lang.ref.WeakReference;

/**
 * Created by Jazib on 8/8/2016.
 */
public class ImageLoader extends Loader<Bitmap> {

    private static final String TAG = "ImageLoader";

    public ImageLoader() {
        memoryCache = new ImageMemoryCache();
    }

    @Override
    public Bitmap decodeItem(InputStream in) {
        Utils.LogD(TAG, "Decoding bitmap");
        Bitmap bitmap = BitmapFactory.decodeStream(in);
        if (bitmap != null) {
            return bitmap;
        }
        return null;
    }

    static class DownloadedDrawable extends ColorDrawable {
        private final WeakReference<Loader.LoaderAsyncTask> bitmapDownloaderTaskReference;

        public DownloadedDrawable(Loader.LoaderAsyncTask bitmapDownloaderTask) {
            super(Color.BLACK);
            bitmapDownloaderTaskReference =
                    new WeakReference<Loader.LoaderAsyncTask>(bitmapDownloaderTask);
        }

        public Loader.LoaderAsyncTask getBitmapDownloaderTask() {
            return bitmapDownloaderTaskReference.get();
        }
    }

    public void loadImage(String url, ImageView imageView, ResponseListener<Bitmap> listener) {
        Bitmap bitmap = memoryCache.get(url);
        if(bitmap != null) {
            Utils.LogD(TAG, "From Cache");
            listener.onSuccesfullResponse(bitmap);
        } else {
            download(url, imageView, listener);
        }
    }

    public void download(String url, ImageView imageView, ResponseListener<Bitmap> listener) {
        if (cancelPotentialDownload(url, imageView)) {
            Loader.LoaderAsyncTask task = new Loader.LoaderAsyncTask(imageView, listener);
            DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
            imageView.setImageDrawable(downloadedDrawable);
            task.execute(url);
        }
    }

    private static boolean cancelPotentialDownload(String url, ImageView imageView) {
        Loader.LoaderAsyncTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.getUrl();
            if ((bitmapUrl == null) || (!bitmapUrl.equals(url))) {
                bitmapDownloaderTask.cancel(true);
            } else {
                // The same URL is already being downloaded.
                return false;
            }
        }
        return true;
    }

    private static Loader.LoaderAsyncTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                final DownloadedDrawable asyncDrawable = (DownloadedDrawable) drawable;
                return asyncDrawable.getBitmapDownloaderTask();
            }
        }
        return null;
    }

    @Override
    public void handleOnPostExecute(Bitmap bitmap,  String url, Loader.LoaderAsyncTask task,  ResponseListener<Bitmap> listener) {
        Utils.LogD(TAG, "in onPostExecute");

        Loader.LoaderAsyncTask bitmapDownloaderTask = getBitmapDownloaderTask((ImageView) task.getImageViewWeakReference().get());
        memoryCache.put(url, bitmap);
        if (task == bitmapDownloaderTask) {
            listener.onSuccesfullResponse(bitmap);
        }

    }

}
