package com.assignment.awesomelibrary.loader;

import android.graphics.Bitmap;

import com.assignment.awesomelibrary.cache.JSONMemoryCache;
import com.assignment.awesomelibrary.cache.MemoryCache;
import com.assignment.awesomelibrary.ResponseListener;
import com.assignment.awesomelibrary.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Jazib on 8/8/2016.
 */
public class JSONArrayLoader extends Loader<JSONArray>{

    private static final String TAG = "JSONArrayLoader";

    public JSONArrayLoader(String url) {
        memoryCache = new JSONMemoryCache();
//        mUrl = url;
    }

    public void loadJSONArray(String url, ResponseListener<JSONArray> listener) {
        JSONArray jsonArray= memoryCache.get(url);
//        mListener = listener;
        if(jsonArray != null) {
            Utils.LogD(TAG, "From Cache");
            listener.onSuccesfullResponse(jsonArray);
        } else {
            load(listener, url);
        }

    }

    @Override
    public JSONArray decodeItem(InputStream in) {
        StringBuilder result = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            JSONArray jsonArray = new JSONArray(result.toString());
            return jsonArray;
        } catch (JSONException e) {
            e.printStackTrace();
           return null;
        }
    }

    @Override
    public void handleOnPostExecute(JSONArray result,  String url, Loader.LoaderAsyncTask task,  ResponseListener<JSONArray> listener) {
        Utils.LogD(TAG, "in Base onPostExecute");
        if(result != null ) {
            listener.onSuccesfullResponse(result);
        }
        memoryCache.put(url, result);
    }

    public MemoryCache getMemoryCache() {
        return memoryCache;
    }
}
